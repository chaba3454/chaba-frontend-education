This project has three simple html pages where I tried to do my best in practicing HTML and CSS.
- page.html contains a simple structure of HTML-page;
- form.html contains a simple input form;
- index.html has a simplest HTML elements.

# Clone your project
Go to your computer's shell and type the following command with your SSH or HTTPS URL:

```sh
$ git clone git@gitlab.com:chaba3454/chaba-frontend-education.git
```
>You can copy a link to the Git repository through a SSH or a HTTPS protocol.

# Live-server

This is a little development server with live reload capability. 
Use it for hacking your HTML/JavaScript/CSS files, but not for deploying the final site.

To install live-server You need **node.js v8.10.0** and **npm v3.5.2**.

Comand to install live-server:
```sh
npm install -g live-server
```
